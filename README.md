#Orlik Search#

1. Zaimportuj plik m4049_orlik-search.sql do swojej bazy MySQL (np. poprzez PhpMyAdmin)
2. Skonfiguruj plik "sql-data.php", który znajduje się w folderze "docs/php/models/". Tam należy podmienić wartości zmiennych na takie, które będą odpowadały Twoim danym konfiguracyjnym serwera.
3. Po skonfigurowaniu pliku przenieś wszystkie pliki (prócz m4049_orlik-search.sql, warto go zachwać dla bezpieczeństwa) do Twojego hostingu lub do folderu "htdocs", jesli będziesz testował go na XAMPP'ie.


###Przykładowa konfiguracja pliku "sql-data.php"###

```
#!php

$host = "jan-kowalski.pl" // to twój host, w XAMPP'ie domyślnie to localhost
$user = "jan-kowalski" //nazwa użytkownika
$pass = "hasło123" //twoje hasło
$db_name = "mja_baza" //nazwa twojej bazy danych

```

testowa strona [orlik-search-alfa.ct8.pl](http://orlik-search-alfa.ct8.pl/) 