-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql.ct8.pl
-- Generation Time: Oct 25, 2016 at 12:31 AM
-- Server version: 5.7.13-log
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m4049_orlik-search`
--

-- --------------------------------------------------------

--
-- Table structure for table `Godziny_otwarcia`
--

CREATE TABLE `Godziny_otwarcia` (
  `ID` int(11) NOT NULL,
  `Dzien` text NOT NULL,
  `Od` time NOT NULL,
  `Do` time NOT NULL,
  `Typ` int(11) NOT NULL COMMENT '1-nożna/2-siatka/3-kosz'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Godziny_otwarcia`
--

INSERT INTO `Godziny_otwarcia` (`ID`, `Dzien`, `Od`, `Do`, `Typ`) VALUES
(1, 'Poniedziałek', '16:00:00', '21:00:00', 1),
(1, 'Środa', '16:00:00', '21:00:00', 1),
(1, 'Czwartek', '16:00:00', '21:00:00', 1),
(1, 'Piątek', '15:00:00', '21:00:00', 1),
(1, 'Sobota', '13:00:00', '20:00:00', 1),
(2, 'Poniedziałek', '15:00:00', '20:00:00', 1),
(2, 'Wtorek', '15:00:00', '20:00:00', 1),
(2, 'Środa', '15:00:00', '20:00:00', 1),
(2, 'Czwartek', '15:00:00', '20:00:00', 1),
(2, 'Piątek', '15:00:00', '20:00:00', 1),
(2, 'Sobota', '13:00:00', '20:00:00', 1),
(3, 'Poniedziałek', '16:00:00', '19:00:00', 1),
(3, 'Wtorek', '16:00:00', '19:00:00', 1),
(3, 'Środa', '16:00:00', '19:00:00', 1),
(3, 'Czwartek', '16:00:00', '19:00:00', 1),
(3, 'Piątek', '16:00:00', '19:00:00', 1),
(3, 'Sobota', '17:00:00', '21:00:00', 1),
(1, 'Wtorek', '15:00:00', '21:00:00', 1),
(7, 'Poniedziałek', '16:00:00', '19:00:00', 1),
(7, 'Wtorek', '16:00:00', '19:00:00', 1),
(7, 'Środa', '16:00:00', '19:00:00', 1),
(7, 'Czwartek', '16:00:00', '19:00:00', 1),
(7, 'Piątek', '16:00:00', '19:00:00', 1),
(7, 'Sobota', '17:00:00', '21:00:00', 1),
(8, 'Poniedziałek', '16:00:00', '19:00:00', 1),
(8, 'Wtorek', '16:00:00', '19:00:00', 1),
(8, 'Środa', '16:00:00', '19:00:00', 1),
(8, 'Czwartek', '16:00:00', '19:00:00', 1),
(8, 'Piątek', '16:00:00', '19:00:00', 1),
(8, 'Sobota', '17:00:00', '21:00:00', 1),
(9, 'Poniedziałek', '16:00:00', '19:00:00', 1),
(9, 'Wtorek', '16:00:00', '19:00:00', 1),
(9, 'Środa', '16:00:00', '19:00:00', 1),
(9, 'Czwartek', '16:00:00', '19:00:00', 1),
(9, 'Piątek', '16:00:00', '19:00:00', 1),
(9, 'Sobota', '17:00:00', '21:00:00', 1),
(4, 'Poniedziałek', '15:00:00', '20:00:00', 1),
(4, 'Wtorek', '15:00:00', '20:00:00', 1),
(4, 'Środa', '15:00:00', '20:00:00', 1),
(4, 'Czwartek', '15:00:00', '20:00:00', 1),
(4, 'Piątek', '15:00:00', '20:00:00', 1),
(4, 'Sobota', '17:00:00', '20:00:00', 1),
(5, 'Poniedziałek', '15:00:00', '20:00:00', 1),
(5, 'Wtorek', '15:00:00', '20:00:00', 1),
(5, 'Środa', '15:00:00', '20:00:00', 1),
(5, 'Czwartek', '15:00:00', '20:00:00', 1),
(5, 'Piątek', '15:00:00', '20:00:00', 1),
(5, 'Sobota', '17:00:00', '20:00:00', 1),
(6, 'Poniedziałek', '15:00:00', '20:00:00', 1),
(6, 'Wtorek', '15:00:00', '20:00:00', 1),
(6, 'Środa', '15:00:00', '20:00:00', 1),
(6, 'Czwartek', '15:00:00', '20:00:00', 1),
(6, 'Piątek', '15:00:00', '20:00:00', 1),
(6, 'Sobota', '17:00:00', '20:00:00', 1),
(1, 'Wtorek', '16:00:00', '21:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `markery2`
--

CREATE TABLE `markery2` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(450) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `anazwa` varchar(450) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Miejscowosc` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Ulica` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `telefon` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Email` text NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `ikona` varchar(60) CHARACTER SET utf8 NOT NULL,
  `Zdjecie` text NOT NULL,
  `Opis` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `markery2`
--

INSERT INTO `markery2` (`id`, `nazwa`, `anazwa`, `Miejscowosc`, `Ulica`, `telefon`, `Email`, `lat`, `lng`, `ikona`, `Zdjecie`, `Opis`) VALUES
(1, 'przy IV Liceum Ogólnokształcącym im. dra Tytusa Chałubińskiego', 'IV Liceum Ogólnokształcące im. dra Tytusa Chałubińskiego', 'Radom', 'Mariacka 25', '(+48) 48 385 19 18', '	szkola@chalubinski.pl', 51.396118, 21.142326, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id1.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(2, 'przy Publicznym Gimnazjum Nr 2 im. Adama Jerzego Czartoryskiego', 'Publiczne Gimnazjum Nr 2 im. Adama Jerzego Czartoryskiego', 'Radom', 'Gagarina 19', '(+48) 48 366 81 83', 'gim2rad@pro.onet.pl', 51.383713, 21.154829, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id2.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(3, 'przy Publicznej Szkole Podstawowej Nr 5 im. Marii Dąbrowskiej', 'Publiczna Szkoła Podstawowa Nr 5 im. Marii Dąbrowskiej', 'Radom', 'Sowińskiego 1', '(+48) 48 364 46 44', 'psp5@radom.home.pl', 51.412888, 21.162279, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id3.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(4, 'przy Publicznym Gimnazjum Nr 5 im. Jacka Malczewskiego', 'Publiczne Gimnazjum Nr 5 im. Jacka Malczewskiego', 'Radom', 'Warszawska 12', '(+48) 48 364 17 32', 'sekretariat@pg5radom.edu.pl', 51.412189, 21.155857, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id4.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(5, 'przy Publicznym Gimnazjum Nr 6', 'Publiczne Gimnazjum Nr 6', 'Radom', 'Sadkowska 16', '(+48) 48 344 89 31', 'gimnazjum6.radom@wp.pl', 51.406601, 21.180614, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id5.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(6, 'przy Publicznym Gimnazjum Nr 8 im. Królowej Jadwigi', 'Publiczne Gimnazjum Nr 8 im. Królowej Jadwigi', 'Radom', 'Piastowska 16', '(+48) 48 333 05 21', 'gim8radom@op.pl', 51.433037, 21.158720, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id6.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(7, 'przy Publicznej Szkole Podstawowej Nr 15 im. Władysława Syrokomli', 'Publiczna Szkoła Podstawowa Nr 15 im. Władysława Syrokomli', 'Radom', 'Kielecka 2/6', '(+48) 48 331 05 70', 'psp15-radom@o2.pl', 51.392780, 21.102037, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id7.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(8, 'przy Publicznej Szkole Podstawowej Nr 21 im. ks. Jana Twardowskiego', 'Publiczna Szkoła Podstawowa Nr 21 im. ks. Jana Twardowskiego', 'Radom', 'Trojańska 5', '(+48) 48 332 29 47', 'sekretariat@psp21.pl', 51.365677, 21.128651, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id8.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(9, 'przy Publicznej Szkole Podstawowej Nr 24 im. Kornela Makuszyńskiego', 'Publiczna Szkoła Podstawowa Nr 24 im. Kornela Makuszyńskiego', 'Radom', 'Powstańców Śląskich 4', '(+48) 48 344 12 27', 'psp24radom@poczta.fm', 51.414707, 21.182819, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id9.png', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa'),
(10, 'przy Publicznej Szkole Podstawowej Nr 1 im. Jana III Sobieskiego ', 'Szydłowiec, ul. Wschodnia 57', 'Szydłowiec', 'Wschodnia 57', '48 617 10 07', 'psp1@szydlowiec.pl', 51.224392, 20.863829, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id10.png', 'Kompleks boisk wielofunkcyjnych o sztucznej nawierzchni, w tym boisko do gry w piłkę nożną i boisko do gier zespołowych.'),
(17, 'w dzielnicy Praga Północ 2 (przy ZSZ Nr 14)', 'Dzielnica Praga Północ 2 (przy ZSZ Nr 14)', 'Warszawa', 'J. Szanajcy 5', '22 619 11 20', 'sekretariat@dosir.waw.pl', 52.261932, 21.026417, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'Obiekt składa się  z :\r\na)     dwóch boisk sportowych \r\n• boisko piłkarskie  o wymiarach 30 m x 62 m (pole gry 26 m \r\nx 56 m), ogrodzone po obwodzie ogrodzeniem o wysokości min. 4 m  z siatki stalowej powlekanej lub z prętów zgrzewanych powlekanych oraz wyposażone w piłkochwyty o wysokości  min. 6 m, wykonane z siatki i montowane  w sposób trwały z podłożem, umieszczone w odległości nie mniejszej niż 1m od ogrodzenia za bramką (piłkochwyty nie są częścią ogrodzenia boisk). Nawierzchnia boiska wykonana na podbudowie dynamicznej. Trawa syntetyczna o wysokości  min. 40 mm. Wyposażenie sportowe trwale montowane do podłoża (dwie bramki do gry w piłkę nożną);\r\n\r\n• boisko wielofunkcyjne do koszykówki i piłki siatkowej o wymiarach 19,1m x 32,1m, ogrodzone po obwodzie ogrodzeniem o wysokości min. 4m, \r\nz siatki stalowej powlekanej lub prętów  zgrzewanych, powlekanych. Nawierzchnia boiska – poliuretanowa, przeznaczona do boisk wielofunkcyjnych, wykonana na podbudowie dynamicznej. Wyposażenie sportowe montowane w sposób trwały z podłożem.\r\n\r\nb) kontener sanitarno – szatniowy spełniający następujące wymogi funkcjonalne:\r\nc) oświetlanie kompleksu\r\nd) 8 słupów oświetleniowych dla boiska piłkarskiego i boiska wielofunkcyjnego.'),
(12, 'w dzielnicy Białołęka', 'Dzielnica Białołęka', 'Warszawa', 'Kowalczyka/ Krzyżówki', '22 676 67 49', 'sekretariat@bos.waw.pl', 52.304768, 20.994122, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boiska piłkarskiego 30m x 62m,\r\nwielofukcyjnego boiska do siatkówki 20m x 30m i do piłki koszykowej 20m x 30m,\r\nzaplecza sanitarnego (Wc dla niepełnosprawnych na wózkach),\r\nogrodzenie o wysokości 4 metry z piłkochwytami za bramkami o wysokości 6 m,\r\noświetlenie,\r\nmonotoring wizyjny (CCTV)'),
(11, '', 'Szydłowiec, ul. Sportowa 9', 'Szydłowiec', 'Sportowa 9', '48 617 86 30', 'urzad@szydlowiec.pl', 51.222385, 20.853403, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '/img/orliki/id10.png', 'Boisko do piłki nożnej - nawierzchnia trawa syntetyczna oraz boisko wielofunkcyjne.'),
(15, 'w dzielnicy Mokotów (przy Zespole Szkół nr 22)', 'Dzielnica Mokotów (przy Zespole Szkół nr 22)', 'Warszawa', 'Kazimierzowska 60', '(22) 56 51 689', 'sport@mokotow.waw.pl', 52.205448, 21.014185, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko do piłki nożnej,\r\nboisko wielofunkcyjne do piłki ręcznej, piłki siatkowej oraz koszykówki,\r\nbudynek kontenerowy sanitarno-szatniowy,\r\noświetlenie boiska\r\nogrodzenie wraz z piłkochwytami.'),
(13, 'w dzielnicy Bielany (przy SP Nr 53)', 'Dzielnica Bielany (przy SP Nr 53)', 'Warszawa', 'Rudzka 6', '(22) 633 86 80', 'biuro@crs-bielany.waw.pl', 52.279999, 20.974531, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko piłkarskie o pow. 1.860 m², (60mx30m)\nboisko wielofunkcyjne (siatkówka/koszykówka) o pow. ok. 613 m²,\nbudynek sanitarno-szatniowy o pow. 60 m² z magazynem sprzętu, zespołem higieniczno - sanitarnym i pomieszczeniem trenera środowiskowego,\nogrodzenie o wysokości 4 m wraz z piłkochwytami o wysokości 6m\noświetlenie boisk - 8 masztów.'),
(14, 'w dzielnicy Mokotów Dolny 2 - Sielce (przy Gimnazjum nr 3)', 'Dzielnica Mokotów Dolny 2 - Sielce (przy Gimnazjum nr 3)', 'Warszawa', 'Chełmska 23', '(22) 56 51 689', 'sport@mokotow.waw.pl', 52.201557, 21.038946, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko do piłki nożnej z piłkochwytami,\r\nboisko do koszykówki i siatkówki,\r\nbieżnia do skoku w dal z piaskownicą,\r\nzaplecze sanitarno-szatniowe (pomieszczenie trenera, magazyn sprzętu, 2 szatnie z zapleczem sanitarnym)\r\noświetlenie,\r\nogrodzenie.'),
(16, 'w dzielnicy Praga Północ 1 (przy Zespole Szkół nr 33)', 'Dzielnica Praga Północ 1 (przy Zespole Szkół nr 33)', 'Warszawa', 'Targowa 86', '22 511 20 00', 'sekretariat@dosir.waw.pl', 52.257465, 21.032639, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'Obiekt składa się z:\r\na)     dwóch boisk sportowych\r\n • boisko piłkarskie  o wymiarach 30 m x 62 m (pole gry 26 m \r\nx 56 m), ogrodzone po obwodzie ogrodzeniem o wysokości min. 4 m  z siatki stalowej powlekanej lub z prętów zgrzewanych powlekanych oraz wyposażone w piłkochwyty o wysokości  min. 6 m, wykonane z siatki i montowane  w sposób trwały z podłożem, umieszczone w odległości nie mniejszej niż 1m od ogrodzenia za bramką (piłkochwyty nie są częścią ogrodzenia boisk). Nawierzchnia boiska wykonana na podbudowie dynamicznej. Trawa syntetyczna o wysokości  min. 40 mm. Wyposażenie sportowe trwale montowane do podłoża (dwie bramki do gry w piłkę nożną);\r\n\r\n • boisko wielofunkcyjne do koszykówki i piłki siatkowej o wymiarach 19,1m x 32,1m, ogrodzone po obwodzie ogrodzeniem o wysokości min. 4m, \r\nz siatki stalowej powlekanej lub prętów  zgrzewanych, powlekanych. Nawierzchnia boiska – poliuretanowa, przeznaczona do boisk wielofunkcyjnych, wykonana na podbudowie dynamicznej. Wyposażenie sportowe montowane w sposób trwały z podłożem.\r\n\r\nb)    budynek  sanitarno-szatniowy\r\n W założeniach spełnia następujące wymogi funkcjonalne:\r\n-       magazyn sprzętu gospodarczo-sportowego,\r\n-       szatnie oddzielnie dla każdej płci lub drużyny,\r\n-        zespół higieniczno-sanitarny,\r\n-       pomieszczenie gospodarza obiektu i trenera środowiskowego.\r\nc)     oświetlanie kompleksu\r\n8 słupów oświetleniowych dla boiska piłkarskiego i boiska wielofunkcyjnego.'),
(18, 'w dzielnicy Praga Południe (przy Gimnazjum nr 23)', 'Dzielnica Praga Południe (przy Gimnazjum nr 23)', 'Warszawa', 'Tarnowiecka 4', '+ 48 797 319 382', 'orlik@g23.edu.pl', 52.235870, 21.105938, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko do piłki nożnej z nawierzchnią z trawy syntetycznej,\r\nboisko wielofunkcyjne do siatkówki i koszykówki z nawierzchnią poliuretanową,\r\nzaplecze sanitarno-szatniowe,\r\nogrodzenie boisk z piłkochwytami, oświetlenie,\r\nbieżnię poliuretanową o długości 60 mb,\r\nzeskocznię do skoku w dal.'),
(19, 'w dzielnicy Targówek (przy SP nr 114)', 'Dzielnica Targówek (przy SP nr 114)', 'Warszawa', 'Remiszewska 40', '0-222-166-756', 'orlik@sp114.edu.pl', 52.272099, 21.054096, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko piłkarskie - pole gry 56m x 26m\r\nboisko do koszykówki, piłki ręcznej, piłki siatkowej, tenisa, badmintona - pole gry 28m x 15m\r\nbudynek techniczny, gdzie mieszczą się przebieralnie\r\nogrodzenie\r\noświetlenie'),
(20, 'w dzielnicy Ursus (przy OSiR)', 'Dzielnica Ursus (przy OSiR)', 'Warszawa', 'Gen. K. Sosnkowskiego 3', '506-878-558', 'osir@osirursus.pl', 52.186588, 20.889429, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko do piłki nożnej,\r\nboisko wielofunkcyjne do piłki koszykowej i siatkowej.\r\nzaplecze szatniowo-sanitarne,\r\nogrodzenie terenu\r\noświetlenie\r\ni zakupiono sprzęt sportowy.'),
(21, 'w dzielnicy Ursynów', 'Dzielnica Ursynów', 'Warszawa', 'Przy Bażantarni 3', '22 443 72 73', 'sport@ursynow.pl', 52.137596, 21.066628, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko piłkarskie 30 m x 62 m (pole gry 26m x 56 m), powierzchnia całej nawierzchni 1860 m², nawierzchnia – trawa syntetyczna, bramki o wymiarach 5 x 2 metry.\r\nboisko wielofunkcyjne (koszykówka/siatkówka), 19,1 x 32,1 m (pole gry 28 x 15 m), powierzchnia całej nawierzchni 613,11 m², nawierzchnia poliuretanowa.\r\nbudynek sanitarno-szatniowy, (rodzaje pomieszczeń: magazyn sprzętu/pomieszczenie trenerów, pomieszczenie ochrony, 2 szatnie z własnym zespołem higieniczno-sanitarnym) – całkowita powierzchnia zabudowy 82,90 m², powierzchnia użytkowa 58,20 m², kubatura 237,91 m³.'),
(22, 'w dzielnicy Wilanów - Zawady (przy Przedszkolu nr 416)', 'Dzielnica Wilanów - Zawady (przy Przedszkolu nr 416)', 'Warszawa', 'Syta 123', '22 44 34 990', 'sport@wilanow.pl', 52.173512, 21.107655, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'kompleks sportowy,\r\nbudynek zaplecza socjalno-szatniowego o powierzchni całkowitej 83 m2 i kubaturze 293 m3,\r\noświetlenie,\r\nmonitoring przy użyciu kamer przemysłowych (CCTV),\r\nogrodzenie,\r\nnagłośnienie,\r\nelektroniczna tablica wyników.'),
(23, 'przy Publicznej Szkole Podstawowej nr.2 z Odziałami integracyjnymi im. Kazimierz Puławskiego', 'Publiczna Szkola Podstawowa nr.2 z Odziałami integracyjnymi im. Kazimierz Puławskiego', 'Warka', 'Polna 17', '48 667 21 86', 'psp2.warka@interia.pl', 51.786514, 21.188334, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '.', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\n'),
(24, 'w dzielnicy Wilanów 2 (Urząd Dzielnicy Wilanów)', 'Dzielnica Wilanów 2 (Urząd Dzielnicy Wilanów)', 'Warszawa', 'św. Urszuli Ledóchowskiej', '22 44 34 990', 'sport@wilanow.pl', 52.156586, 21.069590, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '', 'boisko do piłki nożnej z trawy syntetycznej (o wymiarach 30 m x 62 m),\r\nboisko wielofunkcyjne do koszykówki i siatkówki o nawierzchni syntetycznej (o wymiarach 19 m x 32 m),\r\nogrodzenie,\r\noświetlenie'),
(25, 'przy Publicznej Szkole Podstawowej nr.1 im. Gabriela Narutowicza', 'Publiczna Szkola Podstawowa nr.1 im. Gabriela Narutowicza', 'Grójec', 'Piłsudskiego 68', '48 664 24 05 ', 'pspnr1@grojecmiasto.pl', 51.863560, 20.877865, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '.', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\n'),
(26, 'przy Publicznej Szkole Podstawowej im. Tadeusza Kościuszki', 'Publiczna Szkola Podstawowa im. Tadeusza Kościużki', 'Maciejowice', 'Tadeusza Kościużki 19', '(0-25) 682 57 14', 'maciejowicesp@wp.pl', 51.694038, 21.555967, 'http://maps.google.com/mapfiles/kml/pal2/icon49.png', '.', 'boisko do piłki nożnej o wymiarach 62 x 30 m, nawierzchnia trawa syntetyczna\r\nboisko do koszykówki o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\nboisko do piłki siatkowej o wymiarach 32 x 19 m, nawierzchnia poliuretanowa\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `markery2`
--
ALTER TABLE `markery2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `markery2`
--
ALTER TABLE `markery2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
