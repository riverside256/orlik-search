$(document).ready(function() {
   $(".search-bar-content").delay(1000).animate({top: "-80px", opacity: 1}, "400ms", "easeOutCubic");
    
    document.title = "Orlik Search";
    $("div.ui.dropdown").dropdown();

    $("button[type=submit]").click(function() {
        if($("input[name=address]").val().length !==0) {
            
            var co_ordinates_main;
            
            $.ajax({
                url: "../docs/php/controllers/co-ordinates.php",
                type: "get",
                data: "address=" + $("input[name=address]").val(),
                success: function(co_ordinates) {
                    var co_ordinate_result = co_ordinates.split(",");

                    var lat = parseFloat(co_ordinate_result[0]),
                        lng = parseFloat(co_ordinate_result[1]);
                    
                    $.ajax({
                        url: "../docs/php/models/map.php",
                        type: "get",
                        data: "lat=" + lat + "&lng=" + lng,
                        success: function(data) {
                            console.log(data);
                            if (window.DOMParser)
                              {
                              parser=new DOMParser();
                              xmlDoc=parser.parseFromString(data,"text/xml");
                              }
                            else // Internet Explorer
                              {
                              xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                              xmlDoc.async=false;
                              xmlDoc.loadXML(data);
                              }
                            
                            console.log(xmlDoc.getElementsByTagName("dane"));
                            
                            $.ajax({
                                url:  "../docs/php/views/search-result.php",
                                type: "get",
                                data: "address=" + $("input[name=address]").val() + "&distance=" + $(".ui.dropdown .item").attr("data-value") + "&co-ordinate=" + data,
                                beforeSend: function() {
                                    main.showSpinner();
                                },
                                success: function(resp) {
                                    main.hideSpinner();
                                    $(main.content).html("");
                                    setTimeout(function() {
                                        $(main.content).delay(main.duration + 100).html(resp);
                                    }, 1500);
                                }
                            });
                        }
                    });
                }
            });
            
        }
        return false;
    });
});
