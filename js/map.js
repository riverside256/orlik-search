var mapa;

function dodajMarker(lat,lng,ikona_url,nazwa,Ulica,Miejscowosc)
{

    var ikona = new GIcon();
    ikona.image = ikona_url;
    ikona.iconAnchor = new GPoint(15, 12);
    ikona.shadow = "http://maps.google.com/mapfiles/kml/pal2/icon49s.png";
    ikona.infoWindowAnchor = new GPoint(15,12);

    var marker	=	new GMarker(new GLatLng(lat,lng),{title: nazwa, icon: ikona});
    var txt = '<h3>Kompleks boisk typu Orlik</h3>przy '+nazwa+' '+Miejscowosc+' '+Ulica;
    mapa.addOverlay(marker);
    return marker;
}

function mapaStart()
{
    if(GBrowserIsCompatible())  
    {
        mapa = new GMap2(document.getElementById('mapa'),{
        mapTypes: [G_NORMAL_MAP,G_HYBRID_MAP,G_SATELLITE_MAP]
        });
        mapa.setCenter(new GLatLng(51.40272359999999, 21.14713329999995), 12);

        GDownloadUrl('../docs/php/models/map.php', function(dane,kodOdpowiedzi)
        {
            if(kodOdpowiedzi==200)
            {
                var xml = GXml.parse(dane);
                var markery = xml.documentElement.getElementsByTagName("marker");
                for(var i=0; i<markery.length; i++)
                {
                    var lat			=	parseFloat(markery[i].getAttribute("lat"));
                    var lng			=	parseFloat(markery[i].getAttribute("lng"));
                    var ikona_url	=	markery[i].getAttribute("ikona");
                    var nazwa		=	markery[i].getAttribute("nazwa");
                    var Ulica		=	markery[i].getAttribute("Ulica");
                    var Miejscowosc	=	markery[i].getAttribute("Miejscowosc");
                    var Email		=	markery[i].getAttribute("Email");
                    var marker		=	dodajMarker(lat,lng,ikona_url,nazwa,Ulica,Miejscowosc);
                }
            }
            else
            {
                alert('Nie mogłem otworzyć pliku map.php');
            }
        });
    }
}
window.onlaod = mapaStart();
window.onbeforeunload = GUnload();