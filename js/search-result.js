$(document).ready(function() {
   $(".search-result-item").addClass("search-result-item--active");
    
   $("div.ui.dropdown").dropdown();
    
   $("button[type=submit]").click(function() {
       $.ajax({
            url:  "../docs/php/views/search-result.php",
            type: "get",
            data: "address=" + $("input[name=address]").val() + "&distance=" + $(".ui.dropdown .item").attr("data-value"),
            beforeSend: function() { main.showSpinner(); },
            success: function(resp) {
                $(main.content).html("");
                setTimeout(function() {
                    main.hideSpinner();
                    $(main.content).delay(main.duration + 100).html(resp);
                }, 1500);
            }
        });
       return false;
   });
    
    $(".search-result-item").on("mouseover", function(e) {
        //tutaj wpisuj kod
        //e posłuży nam do wyciągnięcia id elementu
        
    })
    .click(function(e) {
        var target = e.target;
        
        if(target.tagName == "H1") {
            var lat_pattern = /[0-9]+\_+[0-9]+/,
                lat = lat_pattern.exec(target.parentElement.id),
                m_lat = lat[0].replace(/\_/g, ".");
            
            var lng_pattern = /[0-9]+\_+[0-9]+$/,
                lng = lng_pattern.exec(target.parentElement.id),
                m_lng = lng[0].replace(/\_/g, ".");
            
            $(".search-result-item").removeAttr("style");
            
            $("#" + target.parentElement.id).removeClass("search-result-item--active");
            setTimeout(function() {
                $(target.parentElement.tagName + ".search-result-item:not(#"+ target.parentElement.id +")").removeClass("search-result-item--active");
                
                setTimeout(function() {
                    $.ajax({
                        url: "../docs/php/views/orlik-description.php",
                        type: "get",
                        data: "lat=" + m_lat + "&lng=" + m_lng,
                        beforeSend: function() {
                            main.showSpinner();
                        },
                        success: function(response) {
                            $(main.content).html("");
                            setTimeout(function() {
                                main.hideSpinner();
                                $(main.content).delay(main.duration + 100).html(response);
                            }, 1500);
                        }
                    });
                }, 100);
                
            }, 400);
        }
    });
    
});