window.onload = function() {
    
    $.ajax({
        url: "../docs/php/views/search-bar.php",
        type: "post",
        success: function(response) {
            main.hideSpinner();
            $(main.content).html(response);
        }
    });

}