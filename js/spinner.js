var main = {
    content: ".content",
    duration: 500,
    hideSpinner: function() {
        $(".loader-spinner-wrapper")
            .delay(main.duration).animate({"z-index": "-9999", opacity: 0});
    },
    showSpinner: function() {
        $(".loader-spinner-wrapper").css("z-index", "9999").animate({opacity: 1});
    }
}