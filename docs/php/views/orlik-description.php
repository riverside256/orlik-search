<?php
    require_once("../models/sql-data.php");

    $sql_message = null;
    $data = new SQLData();

    $conn = $data->connect();

    if($conn != null) {
        
        if(isset($_GET["lat"]) && isset($_GET["lng"])) {
            $lat = floatval($_GET["lat"]);
            $lng = floatval($_GET["lng"]);
            
            $data = $conn->query("SELECT * FROM `markery2` WHERE `lat`=$lat AND `lng`=$lng");
        }
        else {
            $sql_message = "NIe podano wartości";
        }
        
    }
    else {
        $sql_message = "nie można połączyć się z bazą danych";
    }

?>

<div class="wrapper orlik-description-wrapper">
    <div class="main-header">
        <div class="logo"><img src="../../../img/logo-big.png" alt="logo" /></div>
        <div class="search-bar">
            <form class="ui form">
                <div class="fields">
                    <div class="field">
                        <input placeholder="Podaj swoją lokalizację..." name="address" type="text" value="<?php echo $address; ?>">
                    </div>
                    <div class="field">
                        <div class="ui buttons">
                            <button type="submit" class="ui right labeled icon blue medium button">
                                Szukaj
                                <i class="right chevron icon"></i>
                            </button>
                            <button type="button" class="ui button search-tools-button">Narzędzia wyszukiwania</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <div class="orlik-description-content">
        <?php
        
        foreach($data as $item) {
        ?>
        
        <div class="orlik-image">
            <img src="../../<?php echo $item["Zdjecie"]; ?>"alt="orlik-background" />
            <div class="orlik-image-mask">
                <h1><?php echo $item["anazwa"]; ?></h1>
                <div class="orlik-description--basic-data">
                    <span>Miejscowość: <b><?php echo $item["Miejscowosc"]; ?></b></span>
                    <span>Ulica: <b><?php echo $item["Ulica"]; ?></b></span>
                </div>
            </div>
        </div>
        
        <div class="orlik-description">
            <table>
                <tr>
                    <th>Opis:</th>
                    <td><?php echo $item["Opis"]; ?></td>
                </tr>
                <tr>
                    <th>Email: </th>
                    <td><?php echo $item["Email"]; ?></td>
                </tr>
                <tr>
                    <th>Telefon: </th>
                    <td><?php echo $item["telefon"]; ?></td>
                </tr>
                <tr>
                    <th>Godziny otwarcia: </th>
                    <td>
                    <?php
            
                        if($conn != null) {
                            $hours = $conn->query("SELECT * FROM `Godziny_otwarcia` WHERE `ID` = '". $item["id"] ."'");
                            
                            foreach($hours as $h_item) {
                                echo "<li><b>". $h_item["Dzien"] ."</b> od <b>". $h_item["Od"] ."</b> do <b>". $h_item["Do"] ."</b></li>";
                            }
                        }
            
                    unset($conn);
                    ?>
                    </td>
                </tr>
            </table>
        </div>
        
        <?php
        }
        
        ?>
    </div>
</div>