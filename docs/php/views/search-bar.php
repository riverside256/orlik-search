<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyC544TmbIzP47-IOW7fy69ocZPt02PIg7I' type='text/javascript'></script>
<div class="wrapper search-bar-wrapper">

    <div class="search-bar__header">
        <div class="search-bar__mask">
            <div class="logo">
                <img src="img/logo-big.png" alt="logo" class="ui image">
            </div>
            <p>
                Interaktywna wyszukiwarka Orlików
            </p>
        </div>
    </div>

    <div class="search-bar-content">
        <div class="search-bar">
            <form class="ui equal width form">
                <div class="fields">
                    <div class="field">
                        <input type="text" placeholder="Podaj swoją miejscowość..." name="address" />
                    </div>
                    <div class="field">
                        <div class="ui selection dropdown">
                            <input type="hidden" name="distance">
                            <i class="dropdown icon"></i>
                            <div class="default text">Wybierz zakres odległości</div>
                            <div class="menu">
                                <div class="item" data-value="0-500">0 - 500m</div>
                                <div class="item" data-value="500-1000">500m - 1km</div>
                                <div class="item" data-value="1000-1500">1km - 1.5km</div>
                                <div class="item" data-value="1500-2000">1.5km - 2km</div>
                                <div class="item" data-value="2000">powyżej 2km</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <button type="submit" class="ui right labeled icon blue medium button">
                        Szukaj
                        <i class="right chevron icon"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../js/search-bar.js?<?php echo time(); ?>"></script>
