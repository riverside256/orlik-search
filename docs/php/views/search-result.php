
<?php
    require_once("../models/sql-data.php");

    $address = $_GET["address"];
    $distance = $_GET["distance"];
    $distance = var_dump($_GET["co-ordinate"]);

    $distance_data = simplexml_load_string($distance);
    print_r($distance_data);

    echo $co_ordinate;

    $co_ordinate_address = file_get_contents("https://maps.googleapis.com/maps/api/geocode/xml?address=". str_replace(" ", "+", $address) ."&key=AIzaSyC544TmbIzP47-IOW7fy69ocZPt02PIg7I");
    $co_ordinate_data = simplexml_load_string($co_ordinate_address) or die ("Nie można załadować pliku");
    $location_data = json_encode($co_ordinate_data->result->geometry->location);
    $location = json_decode($location_data, true);
      
    $data = new SQLData();
    $conn = $data->connect();

    if($conn != null) {
        
        $data = $conn->query("SELECT * from `markery2`");
        
        $rowNum = $data->rowCount();
        
    }
    else {
        echo "Nie udało się połączyć z bazą danych";
    }
    
    unset($conn);
?>

<script type="text/javascript">
    $("title").html("<?php echo $address; ?> - Orlik Search");
</script>

<div class="wrapper search-result-wrapper">
    <div class="main-header">
        <div class="logo"><img src="../../../img/logo-big.png" alt="logo" /></div>
        <div class="search-bar">
            <form class="ui form">
                <div class="fields">
                    <div class="field">
                        <input placeholder="Podaj swoją lokalizację..." name="address" type="text" value="<?php echo $address; ?>">
                    </div>
                    <div class="field">
                        <div class="buttons">
                            <button type="submit" class="ui right labeled icon blue medium button">
                                Szukaj
                                <i class="right chevron icon"></i>
                            </button>
                            <div class="ui pointing dropdown item button search-result-tools--btn">Narzędzia wyszukiwania</div>
                        </div>
                        <div class="search-result-tools">
                            <div class="twelve wide field">
                                <input type="text" name="street" placeholder="Podaj ulicę..." />
                            </div>
                            <div class="ui selection dropdown">
                                <input type="hidden" name="distance">
                                <i class="dropdown icon"></i>
                                <div class="default text">Wybierz zakres odległości</div>
                                <div class="menu">
                                    <div class="item" data-value="0-500">0 - 500m</div>
                                    <div class="item" data-value="500-1000">500m - 1km</div>
                                    <div class="item" data-value="1000-1500">1km - 1.5km</div>
                                    <div class="item" data-value="1500-2000">1.5km - 2km</div>
                                    <div class="item" data-value="2000">powyżej 2km</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <div class="search-result-content">
        
        <div class="search-result-content--description">
            <?php
                if($rowNum != 0) {
                    echo "<h3>Wyniki wyszukiwania dla <b>\"$address\"</b></h3>";
                    echo "<p>Około $rowNum wyników";
                }
                else {
                    echo "<h2>Brak wyników dla \"<b>". $address ."</b>\"</h2>";
                    echo "<p>Sprawdź, czy poprawnie wpisałeś odpowiednie dane.</p>";
                }
            ?>
        </div>
        
        <div class="search-result">
            <?php

                if($rowNum != 0) {
                    
                    echo "<div class=\"search-result-items\">";
                    
                    $i = 0;
                    
                    foreach($data as $item) {
                        ?>
                        <div class="ui blue segment search-result-item" id="choice-<?php echo str_replace(".", "_", $item["lat"]) . "-" . str_replace(".", "_", $item["lng"]); ?>" style="transition-delay: .<?php echo $i; ?>s">
                                <h1><?php echo $item["anazwa"]; ?></h1>
                                <div class="divider"></div>
                                <div class="search-result-item--description">
                                    <span>Miejscowosć: <b><?php echo $item["Miejscowosc"]; ?></b></span>
                                    <span>Ulica: <b><?php echo $item["Ulica"]; ?></b></span>
                                </div>
                            </div>
                        <?php
                        $i++;
                    }

                    echo "</div>";

                    ?>
                    <div class="search-result-map">
    
                        <div id='mapa' style='width: 100%; height: 400px; border: 1px solid black; background: gray;'>
                        </div>
                        <script>
                        var myLatLng = {lat: <?php print_r(floatval($location["lat"])); ?>, lng: <?php echo print_r(floatval($location["lng"])) ?>}; 
                        function dodajMarker(lat,lng,ikona_url,nazwa,Ulica,Miejscowosc)
                        {

                            var ikona = new GIcon();
                            ikona.image = ikona_url;
                            ikona.iconAnchor = new GPoint(15, 12);
                            ikona.shadow = "http://maps.google.com/mapfiles/kml/pal2/icon49s.png";
                            ikona.infoWindowAnchor = new GPoint(15,12);
                            var txt = '<h3>Kompleks boisk typu Orlik</h3>'+nazwa+' '+Miejscowosc+' '+Ulica;
                             var marker	=	new GMarker(new GLatLng(lat,lng),{title: nazwa, icon: ikona});
                            mapa.addOverlay(marker);
                          GEvent.addListener(marker,"click",function()
	                                {
	                                    marker.openInfoWindowHtml(txt);
                              	});
                            return marker;
                        }
                          function initMap() {
                               
                                if(GBrowserIsCompatible())  
                                {
                                    mapa = new GMap2(document.getElementById('mapa'),{
                                    mapTypes: [G_NORMAL_MAP,G_HYBRID_MAP,G_SATELLITE_MAP]
                                    });
                                    mapa.setCenter(new GLatLng (51.40272359999999, 21.14713329999995), 12);
                                    GDownloadUrl('docs/php/models/map.php', function(dane,kodOdpowiedzi)
                                    {
                                        if(kodOdpowiedzi==200)
                                        {
                                            var xml = GXml.parse(dane);
                                            var markery = xml.documentElement.getElementsByTagName("marker");
                                            for(var i=0; i<markery.length; i++)
                                            {
                                                var lat			=	parseFloat(markery[i].getAttribute("lat"));
                                                var lng			=	parseFloat(markery[i].getAttribute("lng"));
                                                var ikona_url	=	markery[i].getAttribute("ikona");
                                                var nazwa		=	markery[i].getAttribute("nazwa");
                                                var Ulica		=	markery[i].getAttribute("Ulica");
                                                var Miejscowosc	=	markery[i].getAttribute("Miejscowosc");
                                                var Email		=	markery[i].getAttribute("Email");
                                                var marker		=	dodajMarker(lat,lng,ikona_url,nazwa,Ulica,Miejscowosc);
                                            }
                                        }
                                        else
                                        {
                                            alert('Nie mogłem otworzyć pliku map.php');
                                        }
                                    });
                                }
                          
                          }     
                            window.onload = initMap();
                            window.onbeforeunload = GUnload();
                        </script>
                    </div>
                    <?php
                }
           ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/search-result.js"></script>