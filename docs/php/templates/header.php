<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/lib/semantic-ui/semantic.min.css" />
        <link rel="stylesheet" type="text/css" href="css/lib/semantic-ui/components/dropdown.css" />
        <link rel="stylesheet" type="text/css" href="css/lib/semantic-ui/components/menu-min.css" />
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css?<?php echo time(); ?>" />
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/search-bar.css?<?php echo time(); ?>" />
        <link rel="stylesheet" type="text/css" href="css/search-result.css?<?php echo time(); ?>" />
        <link rel="stylesheet" type="text/css" href="css/orlik-description.css?<?php echo time(); ?>" />
    </head>
    <body>
